<?php

/**
 * Security: Block direct access.
 */
defined( 'ABSPATH' ) || die( 'No script kiddies please!' );


/**
 * Update Ninja Form values if field exists.
 * - Place this code in your themes functions.php file.
 * - You will need a field 'Label' named 'user_agent' in your form for this to populate.
 * - If you want to know if they are on a mobile dveice. You will need a field 'Label' named 'mobile_device'.
 *
 * @param String $default_value - Set default value of the form field.
 * @param String $field_type - Value of the form field type.
 * @param Array  $field_settings - Values associated with the field itself.
 */
function populate_nf_values( $default_value, $field_type, $field_settings ) {
	//print 'DV: ' . $default_value . ' - Field Type: ' . $field_type . ' - Field Settings: '; print_r( $field_settings ); print '<br>'; // DEBUG
	$field_value = mb_strtolower( $field_settings['label'], 'UTF-8' );

	// User's User Agent.
	// Potential Form Field Names.
	$field_name_user_agent = array(
		'user agent',
		'user-agent',
		'user_agent',
	);
	foreach ( $field_name_user_agent as $potential_ua_field_name ) {
		if ( 'hidden' === $field_type && $potential_ua_field_name === $field_value && isset( $_SERVER['HTTP_USER_AGENT'] ) ) {
			return sanitize_text_field( wp_unslash( $_SERVER['HTTP_USER_AGENT'] ) );
		}
	}

	// Attempt to detect if the user is on a mobile device. Note: This is based off the browsers user agent.
	// Potential Form Field Names.
	$field_name_mobile_device = array(
		'mobile device',
		'mobile-device',
		'mobile_device',
		'user mobile',
		'user-mobile',
		'user_mobile',
		'mobile',
	);
	foreach ( $field_name_mobile_device as $potential_md_field_name ) {
		if ( 'hidden' === $field_type && wp_is_mobile() && $potential_md_field_name === $field_value ) {
			return 'True';
		} elseif ( 'hidden' === $field_type && ! wp_is_mobile() && $potential_md_field_name === $field_value ) {
			return 'False';
		}
	}

}
add_filter( 'ninja_forms_render_default_value', 'populate_nf_values', 10, 3 );