<?php


/**
 * Undocumented Ninja Forms Dynamic Redirect w/Parameters
 * - Author: David Komando
 * - Version: 1.0.0
 * - Date: 2020-06-19
 * - Description: This code snippet will need to placed in your functions.php & 
 *                you will need to enable the "Redirect" action in your 
 *               "Emails & Actions" and enter a base URL to use. This also 
 *               checks your forms hidden input field parameters to see if they  
 *               were originally populated by the URI.
 *
 * - Additional Info: I wrote this due to not always having these tracking 
 *                    parameters populated, which left the redirected URI full 
 *                    of unnecessary garbage! I was looking for a more 
 *                    professional appearance than NF offered by default. This 
 *                    could also be easily used for other custom scenarios.
 *                    Also, its nice to have options you can control.
 * 
 * - Problem:
 * -- https://ninjaforms.com/docs/querystrings/
 * --- Default NF Example: /thank-you/?utm_source=test&utm_medium={utm_medium}&utm_campaign={utm_campaign}&utm_id={utm_id}&utm_term={utm_term}
 * --- Fixed NFDR Example: /thank-you/?utm_source=test
 */

// Global Variables
$redirect_params = '';

// Collect All Our Tracking Parameters
function collect_ninja_forms_tracking_params( $form_data ) {
	global $redirect_params;

	foreach( $form_data['fields'] as $field ) {
		$utm_matches = [];
		if ( preg_match( '/(utm_[a-z]*)|(gclid)/', $field['key'], $utm_matches ) ) {
			if ( (count($utm_matches) > 0) && (strpos( $field['value'], '{' ) === false) ) {
				// This will update the global variable each iteration
				$redirect_params = add_query_arg( sanitize_text_field( $utm_matches[0] ), sanitize_text_field( $field['value'] ), $redirect_params );
			}
		}
	}
	unset($field);

	return $form_data;
}
add_filter( 'ninja_forms_submit_data', 'collect_ninja_forms_tracking_params' );


// Rewrite Ninja Forms Action For This Submission Only!
function rewrite_ninja_forms_redirect_action( $actions, $form_data ) {
	global $redirect_params;

	foreach( $actions as $action_key => $action ) {
		if ( $action['settings']['type'] === 'redirect' ) {
			// Keep the front-end entered baseurl path and append collected parameters
			$actions[$action_key]['settings']['redirect_url'] = $action['settings']['redirect_url'] . $redirect_params;
		}
	}
	unset($action);
	
	return $actions;
}
add_filter( 'ninja_forms_submission_actions', 'rewrite_ninja_forms_redirect_action', 10, 2 );
